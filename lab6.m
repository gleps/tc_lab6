%% BPSK
h = modem.pskmod('M', 2);
msg = randint(20, 1, 2);
mod_sig = modulate(h, msg);

plot_range = 1:20;
img = stem(plot_range, msg);

img = scatterplot(mod_sig, 'LineWidth', 5);
saveas(img, 'report/fig/bpsk.png');

%% PSK
h = modem.pskmod('M', 8);
msg = randint(20, 1, 8);
mod_sig = modulate(h, msg);

plot_range = 1:20;
img = stem(plot_range, msg);

img = scatterplot(mod_sig);
saveas(img, 'report/fig/psk.png');

%% OQPSK
h = modem.oqpskmod;
msg = randint(20, 1, 4);
mod_sig = modulate(h, msg);

plot_range = 1:20;
img = stem(plot_range, msg);

img = scatterplot(mod_sig);
saveas(img, 'report/fig/oqpsk.png');

%% genQAM
h = modem.genqammod;
msg = randint(20, 1, 8);
mod_sig = modulate(h, msg);

plot_range = 1:20;
img = stem(plot_range, msg);

img = scatterplot(mod_sig);
saveas(img, 'report/fig/genqam.png');

%% MSK
h = modem.mskmod;
msg = randint(20, 1, 2);
mod_sig = modulate(h, msg);

plot_range = 1:20;
img = stem(plot_range, msg);

img = scatterplot(mod_sig);
saveas(img, 'report/fig/msk.png');

%% M-FSK
msg = randint(100, 1, 8);
inp.time = 0.01:0.01:1;
inp.signals.values = msg;
inp.signals.dimensions = 1;
sim('mfskmod');
img = scatterplot(mod_sig);
saveas(img, 'report/fig/mfsk.png');
